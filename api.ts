export const fetchProducts = async () => {
    try {
      const data = require('./src/data/items.json');
      return data;
    } catch (error) {
      console.error('Error fetching data:', error);
      throw error;
    }
  };
  