Note: 
Make sure you're enviroment is ready for react native development.
If not, follow guide in https://reactnative.dev/docs/environment-setup.


Open a terminal

1. run  "yarn install" or "npm install" to install all the packages used in the app.

2. 
ANDROID
   - run "npm run android" or "yarn android" or "react-native run-android" .

IOS

   - you need to have an apple development account.
   - if you have apple development account, 
        create new identifier for this app or 
        you can use the existing and just use the bundleid from your other ios app
        and use it for testing
   - run "npm run pod-install" or "yarn pod-install" or "cd ios && pod install && cd .."
   - then run "npm run ios" or "yarn ios" or "react-native run-ios"
        





Feel free to contact me if having problem.