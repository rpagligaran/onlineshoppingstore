import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Provider } from 'react-redux';
import {store} from './src/store';
import RootNavigator from './src/navigation';

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <RootNavigator/>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
