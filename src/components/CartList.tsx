import React, { useState } from 'react';
import { FlatList, Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { CartItem } from '../../types';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';

interface CartListProps {
  cartItems: CartItem[];
  deductQty: any;
  editQty: any; 
  addQty: any; 
  removeItem: any;
}

const CartList : React.FC<CartListProps> = ({cartItems, deductQty, editQty, addQty, removeItem}) => {
  const [onEditIndex, setOnEditIndex] = useState(-1)

  const renderItem = ({ item, index }: { item: CartItem; index: number }) => (
    <View style={{ marginBottom: 10, borderRadius: 10 }} key={index}>
      <View style={{ width: "100%", flexDirection: 'row' }}>
        <Image source={{ uri: item.imageUrl }} style={styles.productImage} />
        <View>
          <Text style={{ fontSize: 18, fontWeight: "bold", color: 'black' }}>{item.productName}</Text>
          <Text style={{ fontSize: 18, fontWeight: "bold", color: 'black' }}>₱ {item.unitPrice.toLocaleString()}</Text>
          <View style={{ marginTop: 10, width: "68%", flexDirection: 'row', justifyContent: 'space-between', alignItems: "center" }}>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: "center" }}>
              <TouchableOpacity onPress={() => deductQty(item)}>
                <View style={{ height: 30, width: 30, justifyContent: "center", borderRadius: 15, borderWidth: 1, alignItems: "center" }}>
                  <AntDesign name={'minus'} size={20} color="black" style={{ fontWeight: "bold" }} />
                </View>
              </TouchableOpacity>

              {
                onEditIndex === index ?
                  <TextInput autoFocus={true} onSubmitEditing={({ nativeEvent }) => editQty(item, parseInt(nativeEvent.text))} keyboardType='decimal-pad' style={{ marginLeft: 13, marginRight: 5 }} />
                  :
                  <TouchableOpacity onPress={() => setOnEditIndex(index)}>
                    <Text style={{ marginHorizontal: 15, fontSize: 15 }}>{item.quantity}</Text>
                  </TouchableOpacity>
              }
              <TouchableOpacity onPress={() => addQty(item)}>
                <View style={{ height: 30, width: 30, justifyContent: "center", borderRadius: 15, borderWidth: 1, alignItems: "center" }}>
                  <Ionicons name={'add'} size={20} color="black" style={{ fontWeight: "bold" }} />
                </View>
              </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={() => removeItem(item.id)}>
              <Ionicons name={'trash'} size={25} color="red" style={{ fontWeight: "bold" }} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );

  return (
    <FlatList
      data={cartItems}
      showsVerticalScrollIndicator={false}
      renderItem={renderItem}
      keyExtractor={(item) => item.id.toString()}
    />
  );
};


const styles = StyleSheet.create({
  productImage: {
    width: 100,
    borderRadius: 10,
    height: 100,
    resizeMode: 'contain',
    marginBottom: 10,
    marginRight: 10,
  },
});
export default CartList;

