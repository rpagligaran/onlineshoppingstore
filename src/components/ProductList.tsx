import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React from 'react';
import { View, Text, FlatList, TouchableOpacity, Image, StyleSheet, Platform } from 'react-native';
import { Item } from '../../types';
import { RootStackParamList } from '../navigation';

interface ProductListProps {
    data: Item[];
}

const ProductList: React.FC<ProductListProps> = ({ data }) => {

    const navigation = useNavigation<StackNavigationProp<RootStackParamList, 'Details'>>();
    const renderItem = ({ item, index }: { item: Item; index: number }) => (
        <View style={[styles.itemContainer, { flex: data.length > 1 ? 1 : .5, }]} key={index}>
            <TouchableOpacity onPress={() => navigation.navigate('Details', { item })}>
                <Image source={{ uri: item.imageUrl }} style={styles.productImage} />
                <View style={styles.productInfo}>
                    <Text style={styles.productName}>{item.productName}</Text>
                    <Text style={styles.unitPrice}>₱ {item.unitPrice.toLocaleString()}</Text>
                </View>
            </TouchableOpacity>
        </View>
    );

    return (
        <FlatList
            data={data}
            showsVerticalScrollIndicator={false}
            renderItem={renderItem}
            keyExtractor={(item) => item.id.toString()}
            numColumns={2}
        />
    );
};

const styles = StyleSheet.create({
    itemContainer: {
        margin: 8,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(30, 30, 31, 0.1)',
                shadowOffset: {
                    width: 0,
                    height: 2,
                },
                shadowRadius: 8,
                shadowOpacity: 1,
            },
            android: {
                elevation: 20,
            },
        }),
        backgroundColor: 'white',
        borderRadius: 10
    },
    productImage: {
        width: '100%',
        borderRadius: 10,
        height: 200,
        resizeMode: 'contain',
        marginBottom: 10,
    },
    productInfo: {
        paddingHorizontal: 5,
        marginBottom: 5
    },
    productName: {
        fontSize: 18,
        fontWeight: "bold",
        color: "black"
    },
    unitPrice: {
        fontSize: 18,
        color: "red"
    },
    productDetails: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 8,
    },
});



export default ProductList;
