// AppNavigator.js

import React from 'react';
import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';

// Import your screens
import BottomTabs from './tabs/BottomTabs';
import ProductDetails from '../screens/ProductDetails';
import { Item } from '../../types';

export type RootStackParamList = {
  Main: undefined;
  Details: {
    item: Item
  };
};

const Stack = createStackNavigator<RootStackParamList>();

const RootNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Main" component={BottomTabs} options={{headerShown: false}} />
      <Stack.Screen name="Details" component={ProductDetails} options={{headerShown: false}} />
    </Stack.Navigator>
  );
};

export default RootNavigator;
