import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Provider, useSelector } from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Text, View, StyleSheet } from 'react-native';
import HomeScreen from '../../screens/Home';
import MyCart from '../../screens/MyCart';
import { getCartItems } from '../../store/cartSlice';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const BottomTabs = () => {
  const cartItems = useSelector(getCartItems);

  return (
    <Tab.Navigator
      screenOptions={{
        tabBarStyle: styles.tabBar,
        tabBarLabelStyle: styles.tabBarLabel,
        tabBarShowLabel: false,
      }}
    >
      <Tab.Screen
        name="Products"
        options={{
          headerShown: false,
          tabBarIcon: ({ focused }) => (
            <View style={[styles.tabIconContainer, { backgroundColor: focused ? 'orange' : 'transparent' }]}>
              <Ionicons name="home" size={styles.tabIcon.fontSize} color={focused ? 'white' : 'gray'} />
            </View>
          ),
        }}
        component={HomeScreen}
      />
      <Tab.Screen
        name="Cart"
        options={{
          headerShown: false,
          tabBarIcon: ({ focused }) => (
            <View style={[styles.tabIconContainer, { backgroundColor: focused ? 'orange' : 'transparent' }]}>
              <Ionicons name="bag" size={styles.tabIcon.fontSize} color={focused ? 'white' : 'gray'} />
              {cartItems.length > 0 && (
                <View style={styles.badgeContainer}>
                  <Text style={styles.badgeText}>{cartItems.length}</Text>
                </View>
              )}
            </View>
          ),
        }}
        component={MyCart}
      />
    </Tab.Navigator>
  );
};


const styles = StyleSheet.create({
  tabBar: {
    height: 80,
    backgroundColor: 'black',
    borderTopWidth: 0,
  },
  tabBarLabel: {
    fontSize: 16,
    borderTopWidth: 0,
  },
  tabIconContainer: {
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabIcon: {
    fontSize: 24,
  },
  badgeContainer: {
    position: 'absolute',
    bottom: 5,
    right: 5,
    width: 20,
    height: 20,
    backgroundColor: 'red',
    borderRadius: 10,
    justifyContent: 'center',
  },
  badgeText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 10,
  },
});


export default BottomTabs;
