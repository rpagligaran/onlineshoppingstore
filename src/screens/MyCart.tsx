import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { RouteProp } from '@react-navigation/native';
import { FlatList, Image, ImageBackground, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { useDispatch, useSelector } from 'react-redux';
import { clearCart, removeItem, getCartItems, addToCart, deductQtyToCart, editQtyToCart, getTotalPrice, getTotalQty } from '../store/cartSlice';
import { CartItem, Item } from '../../types';
import Modal from 'react-native-modal';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../navigation';
import CartList from '../components/CartList';

const MyCart = () => {
    const navigation = useNavigation<StackNavigationProp<RootStackParamList, 'Main'>>();
    const dispatch = useDispatch();
    const cartItems = useSelector(getCartItems);
    const totalPrice = useSelector(getTotalPrice);
    const totalQty = useSelector(getTotalQty);
    console.log(totalQty, "totalQty");
    

    const [onEditIndex, setOnEditIndex] = useState(-1)
    const [isModalVisible, setModalVisible] = useState(false);

    const _removeItem = (id: string) => {
        dispatch(removeItem(id));
    }

    const _clearCart = () => {
        dispatch(clearCart());
    }

    const addQty = (item: CartItem) => {
        dispatch(addToCart(item));
    }

    const editQty = (item: CartItem, quantity: number) => {
        const cartItem: CartItem = {
            ...item, quantity
        }

        dispatch(editQtyToCart(cartItem));
    }

    const deductQty = (item: CartItem) => {
        dispatch(deductQtyToCart(item));
    }


    const checkout = () => {
        setModalVisible(!isModalVisible);
    };

    const continueShopping = ()=>{
        setModalVisible(false);
        navigation.goBack();
        _clearCart();
    }

    
    return (
        <SafeAreaView style={styles.container}>
            <View style={{ flex: 1, margin: 5, borderRadius: 30, backgroundColor: 'white', }}>
                <View style={{ flex: 1, padding: 20, paddingTop: 25, justifyContent: "space-between" }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View />
                        <View />
                        <View>
                            <Text style={{ textAlign: "center", fontSize: 25, fontWeight: "600", color: "black" }}>My Cart</Text>
                        </View>
                        <TouchableOpacity onPress={() => _clearCart()}>
                            <Text style={{ color: 'orange', fontSize: 18 }}>Clear Cart</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, marginTop: 20 }}>
                        <CartList cartItems={cartItems} addQty={addQty} deductQty = {deductQty} editQty={editQty} removeItem={_removeItem}  />
                    </View>
                    <View>
                        <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                            <Text style={{ fontSize: 18 }}>Total Items: </Text>
                            <Text style={{ fontSize: 25, fontWeight: "bold", color: 'black' }}>{totalQty.toLocaleString()}</Text>
                        </View>
                        <View style={{ flexDirection: "row", justifyContent: "space-between", marginBottom: 20, alignItems: "center" }}>
                            <Text style={{ fontSize: 18 }}>Total Price: </Text>
                            <Text style={{ fontSize: 25, fontWeight: "bold", color: 'black' }}>{totalPrice.toLocaleString()}</Text>
                        </View>
                        <TouchableOpacity disabled={cartItems.length === 0} onPress={() => checkout()}>
                            <View style={{ padding: 20, paddingHorizontal: 40, backgroundColor: cartItems.length === 0 ? "#ffd7b5" : 'orange', borderRadius: 20 }}>
                                <Text style={{ color: 'white', fontSize: 18, textAlign: "center" }}>Checkout</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>

            </View>
            <Modal isVisible={isModalVisible} style={{ justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ alignItems: "center", justifyContent: "space-evenly", backgroundColor: 'white', padding: 16, borderRadius: 10, width: 300, height: 300 }}>

                    <Ionicons name="bag-check" size={80} color="orange" style={{ marginBottom: 10, }} />
                    <Text style={{ margin: 5, textAlign: "center", fontSize: 22, color: 'black' }}>Thank you for purchasing.</Text>
                    <TouchableOpacity onPress={() => continueShopping()}>
                        <View style={{marginTop: 20, padding: 10, paddingHorizontal: 40, backgroundColor: 'orange', borderRadius: 20 }}>
                            <Text style={{ color: 'white', fontSize: 18, textAlign: "center" }}>Continue Shopping</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </Modal>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black'
    }
});

export default MyCart;
