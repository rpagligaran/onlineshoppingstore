import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react';
import { RouteProp } from '@react-navigation/native';
import { ImageBackground, Platform, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { RootStackParamList } from '../navigation';
import { useDispatch } from 'react-redux';
import { addToCart } from '../store/cartSlice';
import { CartItem, Item } from '../../types';
import { StackNavigationProp } from '@react-navigation/stack';

type DetailsScreenRouteProp = RouteProp<RootStackParamList, 'Details'>;

interface DetailsScreenProps {
    route: DetailsScreenRouteProp;
}

const ProductDetails: React.FC<DetailsScreenProps> = ({ route }) => {
    const navigation = useNavigation<StackNavigationProp<RootStackParamList, 'Main'>>();
    const dispatch = useDispatch();
    const { item } = route.params;

    const _addToCart = (item: Item) => {
        const cartItem: CartItem = {
            id: item.id,
            productName: item.productName,
            imageUrl: item.imageUrl,
            category: item.category,
            unitPrice: item.unitPrice,
            quantity: 1,
        }

        navigation.navigate('Main');
        dispatch(addToCart(cartItem));
    }

    return (
        <SafeAreaView style={styles.container}>
            {/* Image Section */}
            <View style={styles.imageContainer}>
                <ImageBackground source={{ uri: item.imageUrl }} resizeMode="contain" style={styles.image}>
                    <View style={styles.imageContent}>
                        {/* Back button and category text */}
                        <View style={styles.header}>
                            <TouchableOpacity onPress={() => navigation.goBack()}>
                                <Ionicons name="arrow-back" size={30} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.categoryText}>{item.category}</Text>
                        </View>

                        {/* Product details */}
                        <View style={styles.productDetailsContainer}>
                            <Text style={styles.productName}>{item.productName}</Text>
                            <Text style={styles.description}>{item.description}</Text>
                        </View>
                    </View>
                </ImageBackground>
            </View>

            {/* Price and Add to Cart Section */}
            <View style={styles.bottomBar}>
                <View style={styles.priceContainer}>
                    <Text style={styles.priceLabel}>Price</Text>
                    <Text style={styles.price}>{item.unitPrice.toLocaleString()}</Text>
                </View>
                <TouchableOpacity onPress={() => _addToCart(item)}>
                    <View style={styles.addToCartButton}>
                        <Text style={styles.addToCartButtonText}>Add to Cart</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black'
    },
    // Image Section Styles
    imageContainer: {
        flex: 0.85,
        margin: 5,
        borderRadius: 30,
        backgroundColor: 'white',
    },
    image: {
        flex: 1,
        justifyContent: 'center',
        borderRadius: 30,
        overflow: 'hidden',
    },
    imageContent: {
        flex: 1,
        padding: 15,
        paddingTop: 25,
        justifyContent: 'space-between',
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    categoryText: {
        color: 'gray',
    },
    productDetailsContainer: {
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 30,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(30, 30, 31, 0.1)',
                shadowOffset: {
                    width: 0,
                    height: 2,
                },
                shadowRadius: 8,
                shadowOpacity: 1,
            },
            android: {
                elevation: 10,
            },
        }),
    },
    productName: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        marginTop: 5,
        marginBottom: 10,
    },
    description: {
        fontSize: 16,
    },

    // Price and Add to Cart Section Styles
    bottomBar: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        position: 'absolute',
        bottom: 20,
        paddingHorizontal: 40,
    },
    priceContainer: {
        flex: 1,
    },
    priceLabel: {
        color: 'white',
        fontSize: 18,
    },
    price: {
        ...Platform.select({
            ios: {
                fontSize: 20,
            },
            android: {
                fontSize: 30,
            },
        }),
        color: 'white',
        fontWeight: 'bold',
    },
    addToCartButton: {
        ...Platform.select({
            ios: {
                padding: 10,
                paddingHorizontal: 20,
                borderRadius: 10,
            },
            android: {
                padding: 20,
                paddingHorizontal: 40,
                borderRadius: 20,
            },
        }),
        backgroundColor: 'orange',
        marginBottom: 20
    },
    addToCartButtonText: {
        color: 'white',
        fontSize: 18,
    },
});

export default ProductDetails;