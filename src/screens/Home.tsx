import React, { useEffect, useState } from 'react';
import { SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import ProductList from '../components/ProductList';
import { Item } from '../../types';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { getCategories, getFilteredItems, getSelectedCategory, setCategory, sortProducts } from '../store/productSlice';
import { useDispatch, useSelector } from 'react-redux';
import { fetchProducts } from '../../api';
import { setProductItems, filterProducts } from '../store/productSlice';

const HomeScreen = () => {
  const dispatch = useDispatch();
  const [products, setProducts] = useState<Item[]>([]);
  const [keyword, setKeyword] = useState('');
  const [sort, setSort] = useState('');
  const categories = useSelector(getCategories);
  const category = useSelector(getSelectedCategory);
  const filtered = useSelector(getFilteredItems);

  const selectCategory = (cat: string) => {
    const _category = cat === category ? '' : cat;
    dispatch(setCategory(_category));
    search(keyword);
  };

  const fetchData = async () => {
    try {
      const data = await fetchProducts();
      console.log(data.length, 'length');

      setProducts(data);
      dispatch(setProductItems(data));
    } catch (error) {}
  };

  const search = (keyword: string) => {
    setKeyword(keyword);
    dispatch(filterProducts(keyword));
  };

  const sortByPrice = () => {
    setSort(sort === '>' ? '<' : '>');
    if (sort) {
      console.log(sort, 'sort');
      dispatch(sortProducts(sort));
    }
  };

  useEffect(() => {
    console.log(sort, 'sort');
  }, [sort]);

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    setProducts(filtered);
  }, [category, filtered]);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.contentContainer}>
        <View style={styles.header}>
          <TouchableOpacity onPress={() => sortByPrice()}>
            {sort === '>' ? <AntDesign name="caretup" color={'orange'} size={20} /> : <AntDesign name="caretdown" size={20} color={'orange'} />}
          </TouchableOpacity>
          <View style={styles.searchContainer}>
            <TextInput style={styles.input} placeholder="Search products..." onChangeText={(keyword) => search(keyword)} />
            <Ionicons name="search" size={24} />
          </View>
        </View>

        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={styles.categoriesContainer}>
            {categories.map((cat, index) => (
              <TouchableOpacity style={styles.categoryButton} key={index} onPress={() => selectCategory(cat)}>
                <View style={[styles.categoryBox, { backgroundColor: category === cat ? 'orange' : 'black' }]}>
                  <Text style={styles.categoryText}>{cat}</Text>
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </ScrollView>

        <ProductList data={products} />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
  },
  contentContainer: {
    flex: 1,
    margin: 5,
    borderRadius: 30,
    backgroundColor: 'white',
    paddingVertical: 10,
  },
  header: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingTop: 8,
    paddingBottom: 16,
  },
  input: {
    flex: 1,
    height: 50,
    borderWidth: 0.3,
    borderRadius: 10,
    marginRight: 8,
    paddingHorizontal: 8,
  },
  categoriesContainer: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  categoryButton: {
    margin: 5,
  },
  categoryBox: {
    padding: 10,
    paddingHorizontal: 20,
    height: 40,
    borderRadius: 10,
    justifyContent: 'center',
  },
  categoryText: {
    color: 'white',
    fontSize: 15,
  },
});

export default HomeScreen;
