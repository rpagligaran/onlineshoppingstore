import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from './index';
import { Item } from '../../types';

interface ProductState {
    products: Item[];
    filteredProducts: Item[];
    selectedCategory: string;
    categories: string[];
}

const initialState: ProductState = {
    products: [],
    filteredProducts: [],
    selectedCategory: '',
    categories: ["groceries", "lifestyle", "cloths", "automotive", "gadgets", "furniture", "toys"]
};

const productSlice = createSlice({
    name: 'product',
    initialState,
    reducers: {
        setProductItems: (state, action: PayloadAction<Item[]>) => {
            state.products = action.payload;
            state.filteredProducts = action.payload;
        },
        filterProducts: (state, action: PayloadAction<string>) => {
            console.log("heere....");

            const keyword = action.payload.toLowerCase();
            console.log(keyword, "key");

            const filtered = state.products.filter((product)=>{
                if(product.category.includes(state.selectedCategory)){
                    console.log("test", state.selectedCategory);
                    
                }
                
                return product.productName.toLowerCase().includes(keyword) && product.category.includes(state.selectedCategory)
            });

            state.filteredProducts = filtered;


        },
        sortProducts: (state, action: PayloadAction<string>) => {
            const sortedProducts = [...state.filteredProducts];
            sortedProducts.sort((a, b) => {
                const order = action.payload === '>' ? 1 : -1;
                return (a.unitPrice - b.unitPrice) * order;
            });

            state.filteredProducts = sortedProducts;
        },
        setCategory: (state, action: PayloadAction<string>) => {
            state.selectedCategory = action.payload;
        },
    },
});

export const { setProductItems, filterProducts, setCategory, sortProducts } = productSlice.actions;

export const getProducts = (state: RootState) => state.product.products;
export const selectFilteredProducts = (state: RootState) => state.product.filteredProducts;
export const getSelectedCategory = (state: RootState) => state.product.selectedCategory;
export const getCategories = (state: RootState) => state.product.categories;

export const getFilteredItems = (state: RootState) => state.product.filteredProducts;
export default productSlice.reducer;
