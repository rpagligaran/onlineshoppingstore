import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { CartItem } from '../../types';
import { RootState } from './index';

interface CartState {
    cartItems: CartItem[];
}

const initialState: CartState = {
    cartItems: [],
};

const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        addToCart: (state, action: PayloadAction<CartItem>) => {
            const existingItem = state.cartItems.find((item) => item.id === action.payload.id);

            if (existingItem) {
                existingItem.quantity += 1;
            } else {
                state.cartItems.unshift({ ...action.payload, quantity: action.payload.quantity });
            }
        },
        editQtyToCart: (state, action: PayloadAction<CartItem>) => {
            const existingItem = state.cartItems.find((item) => item.id === action.payload.id);

            if (existingItem) {
                existingItem.quantity = action.payload.quantity;
            }
        },
        deductQtyToCart: (state, action: PayloadAction<CartItem>) => {
            const existingItem = state.cartItems.find((item) => item.id === action.payload.id);

            if (existingItem) {
                existingItem.quantity -= 1;
            }
        },
        removeItem: (state, action: PayloadAction<string>) => {
            state.cartItems = state.cartItems.filter((item: CartItem) => item.id !== action.payload);
        },
        clearCart: (state) => {
            state.cartItems = [];
        },
    },
});


export const { addToCart, deductQtyToCart, editQtyToCart, removeItem, clearCart } = cartSlice.actions;

export const getCartItems = (state: RootState) => state.cart.cartItems;

export const getTotalPrice = (state: RootState) => {
    return getCartItems(state).reduce((total, item) => total + item.unitPrice * item.quantity, 0);
};

export const getTotalQty = (state: RootState) => {
    return getCartItems(state).reduce((totalQty, item) => totalQty + item.quantity, 0);
};

export default cartSlice.reducer;
