import {combineReducers} from 'redux';
import cartSlice from './cartSlice';
import productSlice from './productSlice';

const rootReducer = combineReducers({
  cart: cartSlice,
  product: productSlice
});

export default rootReducer;
